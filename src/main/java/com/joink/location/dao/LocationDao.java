package com.joink.location.dao;

import com.joink.location.client.QueryClient;
import com.joink.location.exception.GenericNotFoundException;
import com.joink.location.mapper.LocationMapper;
import com.joink.location.model.Location;
import com.joink.location.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ChrisCone on 8/15/15.
 */
@Repository
public class LocationDao {

    //TODO queries should be param somehow/where

    String db_schema = "joink_dev";

    @Autowired
    protected JdbcTemplate jdbc;

    @Autowired
    protected QueryClient queryClient;

    public Location getLocation(long id) {
        String query = "SELECT * FROM joink_dev.LOCATION WHERE location_id=?";
        Location location;
        try {

            location = jdbc.queryForObject(query, new Object[]{id}, new LocationMapper());

            if (location == null) {
                throw new GenericNotFoundException("Unable to find location with id = ", id);
            }
        }catch(EmptyResultDataAccessException e){
            throw new GenericNotFoundException("Unable to find location with id = ", id);
        }
        return location;
    }

    public List<Location> getLocations(long[] ids) {
        List<Location> locations;
        List<Long>idList = new ArrayList<>();
        String query = "SELECT * FROM joink_dev.LOCATION WHERE location_id IN (:ids)";
        try {

            for(long i : ids){
                idList.add(i);
            }

            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("ids", Arrays.asList(idList.toArray()));

            NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbc);
            locations = namedParameterJdbcTemplate.query(query,
                    parameters,
                    new LocationMapper());

            if (locations == null || locations.size() == 0) {
                throw new GenericNotFoundException("Unable to find any location with ids = " + idList.toArray());
            }
        }catch (EmptyResultDataAccessException e){
            throw new GenericNotFoundException("Unable to find location any location with ids = " + idList.toArray());
        }
        return locations;
    }

    public List<Location> getAllLocations() {
        List<Location> locations;
        String query = "SELECT * FROM joink_dev.LOCATION";
        try {
            locations = jdbc.query(query, new LocationMapper());

            if (locations == null || locations.size() == 0) {
                throw new GenericNotFoundException("No locations found.");
            }
        }catch (EmptyResultDataAccessException e){
            throw new GenericNotFoundException("Unable to find locations.");
        }
        return locations;
    }

    public Location createLocation(Location location) {
        String[]columns = {"name","address1","address2","zipCode","stateCode","stateName","city","county","phone","areaCode","gLocationId"};

        List<String>cols = new ArrayList<String>();
        for(String column : columns){
            cols.add(column);
        }
        String query = "INSERT INTO joink_dev.LOCATION (name,address1,address2,zip_code,state_code,state,city,phone,area_code,google_place_id)" +
                "VALUES(?,?,?,?,?,?,?,?,?,?) RETURNING location_id";

        long id = jdbc.queryForObject(query, new Object[]{location.getName(), location.getAddress1(), location.getAddress2(),
                location.getZipCode(), location.getStateCode(), location.getStateName(),
                location.getCity(), location.getPhone(), location.getAreaCode(), location.getgLocationId()}, Long.class);
        Location createdLocation = getLocation(id);
        return createdLocation;
    }

    public Location updateLocation(Location location) {
        String query = "UPDATE joink_dev.LOCATION SET" +
                " name=?,address1=?,address2=?,zip_code=?,state_code=?,state=?,city=?,phone=?,area_code=?,google_place_id=?" +
                " where location_id=?";
        long id = location.getId();
        jdbc.update(query, new Object[]{location.getName(), location.getAddress1(), location.getAddress2(),
                location.getZipCode(), location.getStateCode(), location.getStateName(),
                location.getCity(), location.getPhone(), location.getAreaCode(), location.getgLocationId(), id});
        Location updatedLocation = getLocation(id);
        return updatedLocation;
    }

    public SuccessResponse deleteLocation(long id) {//TODO should this be a logical delete (flag flip) or true delete
        String query = "DELETE FROM joink_dev.LOCATION where location_id=?";

        jdbc.update(query, new Object[]{id});
        return new SuccessResponse("Location " +id+ " successfully deleted");
    }
}
