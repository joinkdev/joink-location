package com.joink.location.client;

import com.joink.location.model.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by ChrisCone on 8/15/15.
 */
@Repository
public class QueryClient {

    String url = "http://localhost:8089/queries";

    public List<Query> getAllQueries(){
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        ResponseEntity responseEntity = restTemplate.getForEntity(url,Query.class);

        if(responseEntity == null || responseEntity.getStatusCode().value() != 200){
            throw new RestClientException("Error reaching the queries service.");
        }
        return (List<Query>)responseEntity.getBody();
    }

    public Query getQueryById(int id){
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        ResponseEntity responseEntity = restTemplate.getForEntity(url+"/"+id,Query.class);

        if(responseEntity == null || responseEntity.getStatusCode().value() != 200 || !responseEntity.hasBody()){
            throw new RestClientException("Error reaching the queries service.");
        }

        return (Query)responseEntity.getBody();
    }
}
