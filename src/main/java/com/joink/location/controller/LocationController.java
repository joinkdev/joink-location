package com.joink.location.controller;

import com.joink.location.dao.LocationDao;
import com.joink.location.exception.GenericNotFoundException;
import com.joink.location.model.Location;
import com.joink.location.model.SuccessResponse;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by ChrisCone on 8/14/15.
 */
@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationDao locationDao;

   @RequestMapping(
           value = "/{id}",
           method = RequestMethod.GET)
   @ResponseBody
   @HystrixCommand(fallbackMethod = "getLocationFallback")
   public ResponseEntity<?>getLocation(@PathVariable("id")long id){

       if (id <= 0) {
           throw new IllegalArgumentException("The 'id' parameter must not be null or empty");
       }

       Location location = locationDao.getLocation(id);

       return ResponseEntity
               .status(HttpStatus.OK)
               .body(location);

   }

    public ResponseEntity<?>getLocationFallback(long id){
        throw new GenericNotFoundException("Error: Database was unreachable or degraded.");
    }

    @HystrixCommand(fallbackMethod = "getLocationsFallback")
    @RequestMapping(
            value = "",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?>getLocations(@RequestParam("ids")long[] ids){

        if (ids == null || ids.length == 0) {
            throw new IllegalArgumentException("The 'ids' parameter must not be null or empty");
        }

        List<Location> locations = locationDao.getLocations(ids);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(locations);

    }

    public ResponseEntity<?>getLocationsFallback(long[] ids){
        throw new GenericNotFoundException("Error: Database was unreachable or degraded.");
    }

    @RequestMapping(
            value = "/all",
            method = RequestMethod.GET)
    @ResponseBody
    @HystrixCommand(fallbackMethod = "getLocationsAllFallback")
    public ResponseEntity<?>getAllLocations(){

        List<Location> locations = locationDao.getAllLocations();

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(locations);

    }

    public ResponseEntity<?>getLocationsAllFallback(){
        SuccessResponse successResponse = new SuccessResponse("Fall back worked!");
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(successResponse);
        //throw new GenericNotFoundException("Error: Database was unreachable or degraded.");
    }

    @RequestMapping(
            value = "",
            method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity addLocation(@RequestBody Location location) {

        if (location == null) {
            throw new IllegalArgumentException("Invalid input object.");
        }

        Location newLocation = locationDao.createLocation(location);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newLocation);
    }

    @RequestMapping(
            value = "",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?>updateLocation(@RequestBody Location location) {

        if (location == null) {
            throw new IllegalArgumentException("Invalid input object.");
        }

        Location updatedLocation = locationDao.updateLocation(location);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(updatedLocation);
    }

    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?>deleteLocation(@PathVariable("id")long id) {

        if (id <= 0) {
            throw new IllegalArgumentException("The 'id' parameter must not be null or empty");
        }

        SuccessResponse successResponse = locationDao.deleteLocation(id);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(successResponse);
    }
}
