package com.joink.location.controller;

import com.joink.location.cache.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ChrisCone on 8/14/15.
 */
@RestController
@RequestMapping("/cache")
public class CacheController {

    @CacheEvict(value = CacheConfig.QUERIES_CACHE, allEntries = true)
    @RequestMapping(
            value = "/<endpoint>",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?>purgeCache(){

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("<message>");

    }
}
