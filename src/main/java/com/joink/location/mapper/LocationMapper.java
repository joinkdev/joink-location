package com.joink.location.mapper;

import com.joink.location.model.Location;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ChrisCone on 8/15/15.
 */
public class LocationMapper implements RowMapper<Location> {
    @Override
    public Location mapRow(ResultSet r, int rowNum) throws SQLException {

        Location location = new Location();
        location.setAddress1(r.getString("address1"));
        location.setAddress2(r.getString("address2"));
        location.setCity(r.getString("city"));
        location.setStateCode(r.getString("state_code"));
        location.setStateName(r.getString("state"));
        location.setName(r.getString("name"));
        location.setId(r.getInt("location_id"));
        location.setZipCode(r.getInt("zip_code"));
        location.setPhone(r.getString("phone"));
        location.setAreaCode(r.getInt("area_code"));
       // location.setgLocationId(r.getInt("g_location_id"));
        location.setId(r.getLong("location_id"));

        return location;
    }
}
