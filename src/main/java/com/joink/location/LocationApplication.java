package com.joink.location;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by ChrisCone on 8/14/15.
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableDiscoveryClient
@EnableCircuitBreaker
@ComponentScan("com.joink.location.*")
public class LocationApplication {

    public static Properties props = new Properties();

    public static void main(String[] args) throws IOException{

        SpringApplication app = new SpringApplication(LocationApplication.class);
        app.setShowBanner(false);
        app.run(args);

        loadProperties("application.properties");
    }

    //props should be accessed via annotations when possible
    public static void loadProperties(String fileName) throws IOException {
        if (props.getProperty("appName") != null)
            return;

        InputStream is = LocationApplication.class.getClassLoader().getResourceAsStream("application.properties");
        if (is != null) {
            props.load(is);
        }
        else {
            // LOG
        }
    }
}
