package com.joink.location.util;

import com.joink.location.exception.GenericNotFoundException;
import com.joink.location.model.ErrorResponse;
import com.joink.location.model.Location;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ChrisCone on 8/14/15.
 */
@ControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ResponseEntity<ErrorResponse> noRequestHandlerFoundExceptionHandler(
            HttpServletRequest request, NoHandlerFoundException e){

        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), e), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = GenericNotFoundException.class)
    public ResponseEntity<ErrorResponse> genericNotFoundExceptionHandler(
            HttpServletRequest request, GenericNotFoundException e){
        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), e), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class, MethodArgumentNotValidException.class,
            HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<?> badRequest(HttpServletRequest request, Exception ex) {

        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }


    @ExceptionHandler(value = {IllegalArgumentException.class, NullPointerException.class})
    ResponseEntity<?> handleBadRequests(HttpServletRequest request, RuntimeException ex){
        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Throwable.class)
    ResponseEntity<?> handleControllerException(HttpServletRequest request, Throwable ex) {
        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), (Exception)ex),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = { SQLException.class})
    public ResponseEntity<?> handleDatabaseError(HttpServletRequest request, SQLException ex) {
        return new ResponseEntity<>(new ErrorResponse(LocationUtil.getFullURL(request), (Exception)ex),HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
