package com.joink.location.exception;

/**
 * Created by ChrisCone on 8/15/15.
 */
public class GenericNotFoundException extends RuntimeException {

    public GenericNotFoundException(Exception e){
        super(e);
    }

    public GenericNotFoundException(String message, long id){
        super(message+id);
    }

    public GenericNotFoundException(String message){
        super(message);
    }
}
