package com.joink.location.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ChrisCone on 7/18/15.
 */
public class LocationUtil {

    //common stuff

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }
}
