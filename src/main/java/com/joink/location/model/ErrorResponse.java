package com.joink.location.model;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;

/**
 * Created by ChrisCone on 8/15/15.
 */
public class ErrorResponse {

    public final String url;
    public final String errorMessage;
    public final String errorCause;
    public final String timeStamp;

    public ErrorResponse(String url, Exception ex) {
        this.url = url;
        this.errorMessage = ex.getLocalizedMessage();
        this.errorCause = String.valueOf(ex.getClass().getName());
        this.timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
