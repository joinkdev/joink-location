package com.joink.location.model;

/**
 * Created by ChrisCone on 8/15/15.
 */
public class Query {

    private int id;
    private String query;
    private String schema;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
