package com.joink.location.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ChrisCone on 9/4/15.
 */
public class SuccessResponse {

    public final String message;
    public final String timeStamp;

    public SuccessResponse(String message){
        this.message = message;
        this.timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
